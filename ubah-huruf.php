<?php
	function ubah_huruf($string){
	//kode di sini
		$huruf = ['a','b','c','d','e','f','g','h','i','j','k','l','m','o','p','q','r','s','t','u','v','w','x','y','z'];
		$newString = "";

		for ($j=0; $j < strlen($string); $j++) { 
			for ($i=0; $i < count($huruf); $i++) { 
				if ($huruf[$i]==$string[$j]) {
					$newString .= $huruf[$i+1];
				}
			}
		}

		return $newString."<br>";
	}

	// TEST CASES
	echo ubah_huruf('wow'); // xpx
	echo ubah_huruf('developer'); // efwfmpqfs
	echo ubah_huruf('laravel'); // mbsbwfm
	echo ubah_huruf('keren'); // lfsfo
	echo ubah_huruf('semangat'); // tfnbohbu

?>
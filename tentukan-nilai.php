<?php
	function tentukan_nilai($number)
	{
	    if ($number<100&&$number>=85) {
	    	$string = "Sangat Baik";
	    }elseif ($number<85&&$number>=70) {
	    	$string = "Baik";
	    }elseif ($number<70&&$number>=60) {
	    	$string = "Cukup";
	    }elseif ($number<60) {
	    	$string = "Kurang";
	    }else{
	    	$string = "Terjadi Kesalahan";
	    }

	    return $string."<br>";
	}

	//TEST CASES
	echo tentukan_nilai(98); //Sangat Baik
	echo tentukan_nilai(76); //Baik
	echo tentukan_nilai(67); //Cukup
	echo tentukan_nilai(43); //Kurang
?>